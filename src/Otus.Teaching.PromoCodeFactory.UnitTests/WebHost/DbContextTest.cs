﻿using System;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost;

public class DbContextTest : IDisposable
{
    protected readonly ApplicationDbContext Context;
    public DbContextTest()
    {
        var options = new DbContextOptionsBuilder<ApplicationDbContext>().UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString()).Options;
        Context = new ApplicationDbContext(options);
        Context.Database.EnsureCreated();
        new EfDbInitializer(Context).InitializeDb();
    }
    public void Dispose()
    {
        Context.Database.EnsureDeleted();
        Context.Dispose();
    }
}