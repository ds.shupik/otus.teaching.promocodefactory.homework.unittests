﻿using System;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Namotion.Reflection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners;

public class SetPartnerPromoCodeLimitAsyncTestsWithDb : DbContextTest
{
    private readonly IRepository<Partner> _partnersRepository;

    public SetPartnerPromoCodeLimitAsyncTestsWithDb()
    {
        _partnersRepository = new EfRepository<Partner>(Context);
    }

    public Partner CreateBasePartner()
    {
        var partner = new Partner
        {
            Name = "Суперигрушки",
            IsActive = true,
            PartnerLimits = new List<PartnerPromoCodeLimit>()
        };

        return partner;
    }

    [Fact]
    public async void SetPartnerPromoCodeLimitAsync_SetLimit_LimitSavedInDb()
    {
        // Arrange
        var partner = CreateBasePartner();
        var partnersController = new PartnersController(_partnersRepository);
        await _partnersRepository.AddAsync(partner);

        var request = new SetPartnerPromoCodeLimitRequest
        {
            EndDate = DateTime.UtcNow.AddDays(1),
            Limit = 1
        };

        // Act
        var result = await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

        // Assert
        var response = Assert.IsType<CreatedAtActionResult>(result);
        var partnerId = response.Value.TryGetPropertyValue<Guid>("id");
        var limitId = response.Value.TryGetPropertyValue<Guid>("limitId");

        var newLimit = (await _partnersRepository.GetByIdAsync(partnerId)).PartnerLimits.Single(x => x.Id == limitId);
        newLimit.Should().NotBeNull();
    }
}